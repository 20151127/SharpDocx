﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace SharpDocx
{
    [Serializable]
    public class SharpDocxCompilationException : Exception
    {
        public string Errors;
        public string SourceCode;

        public SharpDocxCompilationException(string code, string errors)
        {
            SourceCode = code;
            Errors = errors;
        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue(nameof(Errors), Errors);
            info.AddValue(nameof(SourceCode), SourceCode);
        }
    }
}