﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SharpDocx.Extensions;

namespace SharpDocx
{
    internal class DocumentAssembly
    {
        private readonly Assembly _assembly;
        private readonly string _className;

        internal DocumentAssembly(
            string viewPath,
            Type baseClass,
            Type modelType)
        {
            if (baseClass == null)
            {
                throw new ArgumentNullException(nameof(baseClass));
            }

            // Load base class assembly.
            var a = Assembly.LoadFrom(baseClass.Assembly.Location);
            if (a == null)
            {
                throw new ArgumentException($"Can't load assembly '{baseClass.Assembly}'", nameof(baseClass));
            }

            // Get the base class type.
            var t = a.GetType(baseClass.FullName);
            if (t == null)
            {
                throw new ArgumentException(
                    $"Can't find base class '{baseClass.FullName}' in assembly '{baseClass.Assembly}'",
                    nameof(baseClass));
            }

            // Check base class type.
            if (t != typeof(DocumentBase) && !t.IsSubclassOf(typeof(DocumentBase)))
            {
                throw new ArgumentException("baseClass should be a DocumentBase derived type", nameof(baseClass));
            }

            // Get user defined namespaces by calling the static DocumentBase.GetNamespaces method.
            var namespaces = Invoke(a, baseClass, nameof(DocumentBase.GetNamespaces));

            // Get user defined using directives by calling the static DocumentBase.GetUsingDirectives method.
            var usingDirectives = Invoke(a, baseClass, nameof(DocumentBase.GetUsingDirectives));

            // Get user defined assemblies to reference.
            var referencedAssemblies = Invoke(a, baseClass, nameof(DocumentBase.GetReferencedAssemblies));

            if (modelType != null)
            {
                // Add namespace(s) of Model and reference Model assembly/assemblies.
                foreach (var type in GetTypes(modelType))
                {
                    if (!string.IsNullOrEmpty(type.Namespace))
                    {
                        // See issue #38.
                        usingDirectives.Add($"using {type.Namespace};");
                    }

                    referencedAssemblies.Add(type.Assembly.Location);
                }
            }

            if (!string.IsNullOrWhiteSpace(baseClass.Namespace))
            {
                namespaces.Add(baseClass.Namespace);
            }

            foreach (var nmspace in namespaces)
            {
                var usingDirective = $"using {nmspace};";

                if (!usingDirectives.Contains(usingDirective))
                {
                    usingDirectives.Add(usingDirective);
                }
            }

            var assemblyFile = baseClass.Assembly.Location;

            if (!referencedAssemblies.Contains(assemblyFile))
            {
                referencedAssemblies.Add(assemblyFile);
            }

            // Create a unique class name.
            _className = $"SharpDocument_{Guid.NewGuid():N}";

            // Create an assembly for this class.
            _assembly = DocumentCompiler.Compile(
                viewPath,
                _className,
                baseClass.Name,
                modelType,
                usingDirectives,
                referencedAssemblies);
        }

        public object Instance()
        {
            return _assembly.CreateInstance($"{DocumentCompiler.Namespace}.{_className}", null);
        }

        private static List<string> Invoke(Assembly a, Type baseClass, string methodName)
        {
            var strs = a.Invoke(baseClass.FullName, null, methodName, null) as IEnumerable<string>;

            if (strs == null)
            {
                return new List<string>();
            }

            if (strs is List<string> list)
            {
                return list;
            }

            return strs.ToList();
        }

        private static IEnumerable<Type> GetTypes(Type type)
        {
#if NET45_OR_GREATER
            if (type.IsConstructedGenericType)
            {
                foreach (var t in type.GenericTypeArguments)
                {
                    yield return t;
                }
            }
#endif
            yield return type;
        }
    }
}