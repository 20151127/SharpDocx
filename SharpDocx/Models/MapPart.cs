﻿namespace SharpDocx.Models
{
    internal readonly struct MapPart
    {
        public MapPart(int startIndex, int endIndex)
        {
            StartIndex = startIndex;
            EndIndex = endIndex;
        }

        public int StartIndex { get; }

        public int EndIndex { get; }
    }
}