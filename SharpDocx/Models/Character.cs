﻿using DocumentFormat.OpenXml;

namespace SharpDocx.Models
{
    public readonly struct Character
    {
        public Character(char ch, OpenXmlElement ele, int index)
        {
            Char = ch;
            Element = ele;
            Index = index;
        }

        public char Char { get; }

        public OpenXmlElement Element { get; }

        public int Index { get; }
    }
}